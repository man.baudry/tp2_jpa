package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


/**
 * Entity implementation class for Entity: Project
 *
 */
@Entity
public class Project {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToMany           
    private List<Employee> employees = new ArrayList<>();
    
    
	public Project() {
		super();
	}
	
	
    public Project(String name) {
		super();
		this.name = name;
	}

    
	public void addEmployee(Employee employee) {
		this.employees.add(employee);
	}
	

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "Project [id=" + id + ", nom=" + name + "]";
	}
   
}
