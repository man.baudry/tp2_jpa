package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Team
 *
 */
@Entity

public class Team {

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
	private String name;
	@OneToMany
	private List<Employee> employees= new ArrayList<>();
	

	public Team() {
		super();
	}

	public Team(String name) {
		super();
		this.name = name;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the employees
	 */
	public List<Employee> getEmployees() {
		return employees;
	}


	/**
	 * @param employees the employees to set
	 */
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}


	@Override
	public String toString() {
		return "Team [id=" + id + ", name=" + name + "]";
	}
   
}
