package models;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Address
 *
 */
@Entity
public class Address {

	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private String street;
	private String city;
	private String postCode;
	

	public Address() { 
		super();
	}
	
	
	public Address(String street, String city, String postCode) {
		super();
		this.street = street;
		this.city = city;
		this.postCode = postCode;
	}


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}


	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}


	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}


	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}


	/**
	 * @return the postCode
	 */
	public String getPostCode() {
		return postCode;
	}


	/**
	 * @param postCode the postCode to set
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}


	@Override
	public String toString() {
		return "Address [id=" + id + ", street=" + street + ", city=" + city + ", postCode=" + postCode + "]";
	}
   
}
