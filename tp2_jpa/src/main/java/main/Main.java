package main;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import models.Address;
import models.Company;
import models.Employee;
import models.Project;
import models.Team;

public class Main {
	private static final String PERSISTENCE_UNIT_NAME = "tp2_jpa"; 
    private static EntityManagerFactory factory;
	
	public static void main(String[] args) {
		
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();

        Company company = new Company("Access-it");
        em.persist(company);
        
        
        Address address = new Address("11 rue Boileux", "Lille", "59800");
        em.persist(address);
        
        
        Project project = new Project("tp jpa");
        em.persist(project);
        
        Project project2 = new Project("tp2 jpa");
        em.persist(project2);
        
        Team team = new Team("m2 miage");
        
        Employee employee = new Employee("Baudry", "Manon", 25); 
        employee.setCompany(company);
        employee.setAddress(address);
        employee.addProjet(project);
        employee.addProjet(project2);
        employee.setTeam(team);
        
        company.getEmployees().add(employee);
        team.getEmployees().add(employee);
        
        em.persist(employee);
        em.persist(company);
        em.persist(team);
        
        displayEmployees(em);
        
        Address address2 = new Address("10 rue des stations", "Lille", "59800");
        em.persist(address2);
        
        Employee employee2 = new Employee("Pereira", "Nicolas", 22); 
        employee2.setCompany(company);
        employee2.setAddress(address2);
        employee2.addProjet(project2);
        employee2.setTeam(team);
        
        company.getEmployees().add(employee2);
        team.getEmployees().add(employee2);
        
        em.persist(employee2);
        em.persist(company);
        em.persist(team);
        
        displayEmployees(em);  
        
        em.getTransaction().commit();
        em.close();
	} 

	
	private static void displayEmployees(EntityManager em) {
		Query q = em.createQuery("select e from Employee e");
        List<Employee> employees = q.getResultList();
        for (Employee employee : employees) {
            System.out.println(employee);
        }
        
        System.out.println("Size: " + employees.size());
	}
}
